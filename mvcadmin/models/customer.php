<?php
class CustomerModel
{
	public static function checkLogin($email, $pass){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM employees WHERE email=:e AND password=:p";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":e",$email);
		$stmt->bindParam(":p",$pass);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$_SESSION['customerid']=$row['id_customer'];
			}
			return true;
		}else{
			return false;
		}
	}

	

	public static function getEmployeesByEmail($email){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM employees WHERE email=:m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$email);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getAllOrder(){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM transactionpay";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$email);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}










public static function getAllCustomer(){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM employees";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}



public static function addCustomerProccess(){
		
			
		$db = DB::getConnection();
			
		
		$sql = "INSERT INTO employees SET LastName=:a ,FirstName=:b ,Title=:c ,Address=:d ,Country=:e ,atm=:f, Phone=:g, Email=:h, Password=:i, Photo=:j, Salary=:k,description=:l";

		//Bước 3: Tiến hành Prepare câu truy vấn
		$stmt = $db->prepare($sql);
		$statusproduct=1;
		$filesName 	= $_FILES['txtFile']['name']; 
		$filesType 	= $_FILES['txtFile']['type'];
		$filesTmp 	= $_FILES['txtFile']['tmp_name'];
		$filesError = $_FILES['txtFile']['error'];
		$filesSize	= $_FILES['txtFile']['size'];
		$status = false;//Biến trạng thái biểu diễn upload thành công hoặc thất bại

		for($i = 0; $i<count($filesName); $i++){
			if(($filesType[$i] == "image/jpeg" || $filesType[$i] == "image/png") && $filesSize[$i] <=20000000 && $filesError[$i] == 0){
				if((move_uploaded_file($filesTmp[$i], "assets/img/".$filesName[$i]))==true){
					$status = true;
				}
			}
		}

		//Bước 4: Truyền giá trị cho các tham số trong câu truy vấn
		$stmt->bindParam(":a",$_POST['txtLastName']);
		$stmt->bindParam(":b",$_POST['txtFirstName']);
		$stmt->bindParam(":c",$_POST['txtTitle']);
		$stmt->bindParam(":d",$_POST['txtAddress']);
		$stmt->bindParam(":e",$_POST['txtCountry']);
		$stmt->bindParam(":f",$_POST['txtAtm']);
		$stmt->bindParam(":g",$_POST['txtPhone']);
		$stmt->bindParam(":h",$_POST['txtEmail']);
		$stmt->bindParam(":i",$_POST['txtPass']);
		$stmt->bindParam(":j",implode(";", $filesName));
		$stmt->bindParam(":k",$_POST['txtSalary']);
		$stmt->bindParam(":l",$_POST['txtDesc']);
		
		//Bước 5: Thực thi câu truy vấn
		$stmt->execute();
		$count = $stmt->rowCount();

		if($status==true){
			if($count>0){
				//echo "<script>alert('Thêm mới thành công');</script>";
				return true;
			}else{
				return false;

			}
		}else{
			return false;
		}
		
		


		
		
		
	}


	public static function deleteCustomer($id){
		
				$db = DB::getConnection();
				
				
				$query = "DELETE FROM employees WHERE id_employee = ?";
				$stmt = $db->prepare($query);
				$stmt->bindParam(1,$id);

				
				
				if($stmt->execute()){
				
					return true;
				}else{
					return false;
				}
				
				

	}
	public static function getCustomerById($id){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM employees WHERE id_employee=:m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$id);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

public static function updateCustomerProccess($id){
		
			
		$db = DB::getConnection();
			
		
		$sql = "UPDATE employees SET LastName=:a ,FirstName=:b ,Title=:c ,Address=:d ,Country=:e ,atm=:f, Phone=:g, Email=:h, Password=:i, Photo=:j, Salary=:k,description=:l WHERE id_employee=:m";

		//Bước 3: Tiến hành Prepare câu truy vấn
		$stmt = $db->prepare($sql);
		$statusproduct=1;
		$filesName 	= $_FILES['txtFile']['name']; 
		$filesType 	= $_FILES['txtFile']['type'];
		$filesTmp 	= $_FILES['txtFile']['tmp_name'];
		$filesError = $_FILES['txtFile']['error'];
		$filesSize	= $_FILES['txtFile']['size'];
		$status = false;//Biến trạng thái biểu diễn upload thành công hoặc thất bại

		for($i = 0; $i<count($filesName); $i++){
			if(($filesType[$i] == "image/jpeg" || $filesType[$i] == "image/png") && $filesSize[$i] <=20000000 && $filesError[$i] == 0){
				if((move_uploaded_file($filesTmp[$i], "assets/img/".$filesName[$i]))==true){
					$status = true;
				}
			}
		}

		//Bước 4: Truyền giá trị cho các tham số trong câu truy vấn
		$stmt->bindParam(":a",$_POST['txtLastName']);
		$stmt->bindParam(":b",$_POST['txtFirstName']);
		$stmt->bindParam(":c",$_POST['txtTitle']);
		$stmt->bindParam(":d",$_POST['txtAddress']);
		$stmt->bindParam(":e",$_POST['txtCountry']);
		$stmt->bindParam(":f",$_POST['txtAtm']);
		$stmt->bindParam(":g",$_POST['txtPhone']);
		$stmt->bindParam(":h",$_POST['txtEmail']);
		$stmt->bindParam(":i",$_POST['txtPass']);
		$stmt->bindParam(":j",implode(";", $filesName));
		$stmt->bindParam(":k",$_POST['txtSalary']);
		$stmt->bindParam(":l",$_POST['txtDesc']);
		$stmt->bindParam(":m",$id);		
		//Bước 5: Thực thi câu truy vấn
		$stmt->execute();
		$count = $stmt->rowCount();

		if($status==true){
			if($count>0){
				//echo "<script>alert('Thêm mới thành công');</script>";
				return true;
			}else{
				return false;

			}
		}else{
			return false;
		}
		
		


		
		
		
	}





}





?>