<?php
/**
 * 
 */
class ProductModel
{
	private $productId;
	private $productName;
	//......

	public static function getAllTransactionPay(){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM transactionpay";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}
	public static function getAllProduct(){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}
	public static function getAllRole(){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM role";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}
	public static function getAllSupplier(){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM supplier";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}
	public static function getAllGender(){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM gender";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}
	public static function addProduct(){
		$db = DB::getConnection();
			
		
			$sql = "INSERT INTO customer SET name=? ,email=? ,password=?,phone=?";
			$stmt = $db->prepare($sql);
			$stmt->bindParam("1",$_POST['txtUser']);
			$stmt->bindParam("2",$_POST['txtMail']);
			$stmt->bindParam("3",$_POST['txtPass']);
			$stmt->bindParam("4",$_POST['txtMobile']);
			
			$stmt->execute();
			$count = $stmt->rowCount();
			if($count>0){
				
				return true;
			}else{
				return false;
			}
		


		
		
		
	}
	public static function addProductProccess(){
		
			
		$db = DB::getConnection();
			
		
		$sql = "INSERT INTO product SET name=:a ,price=:b ,sale=:c ,image=:d ,modem=:e ,id_role=:f, quantity=:g, id_gender=:h, Color=:i, size=:j, description=:k,id_supplier=:l,status=:m";

		//Bước 3: Tiến hành Prepare câu truy vấn
		$stmt = $db->prepare($sql);
		$statusproduct=1;
		$filesName 	= $_FILES['txtFile']['name']; 
		$filesType 	= $_FILES['txtFile']['type'];
		$filesTmp 	= $_FILES['txtFile']['tmp_name'];
		$filesError = $_FILES['txtFile']['error'];
		$filesSize	= $_FILES['txtFile']['size'];
		$status = false;//Biến trạng thái biểu diễn upload thành công hoặc thất bại

		for($i = 0; $i<count($filesName); $i++){
			if(($filesType[$i] == "image/jpeg" || $filesType[$i] == "image/png") && $filesSize[$i] <=20000000 && $filesError[$i] == 0){
				if((move_uploaded_file($filesTmp[$i], "C:/xampp1/htdocs/projectcuoikhoa/projectcuoikhoa/mvc/assets/images/".$filesName[$i]))==true){
					$status = true;
				}
			}
		}

		//Bước 4: Truyền giá trị cho các tham số trong câu truy vấn
		$stmt->bindParam(":a",$_POST['txtName']);
		$stmt->bindParam(":b",$_POST['txtPrice']);
		$stmt->bindParam(":c",$_POST['txtSale']);
		$stmt->bindParam(":d",implode(";", $filesName));
		$stmt->bindParam(":e",$_POST['txtModem']);
		$stmt->bindParam(":f",$_POST['cbRole']);
		$stmt->bindParam(":g",$_POST['txtQuantity']);
		$stmt->bindParam(":h",$_POST['cbGender']);
		$stmt->bindParam(":i",$_POST['txtColor']);
		$stmt->bindParam(":j",$_POST['txtSize']);
		$stmt->bindParam(":k",$_POST['txtDesc']);
		$stmt->bindParam(":l",$_POST['cbSupplier']);
		$stmt->bindParam(":m",$statusproduct);
		//Bước 5: Thực thi câu truy vấn
		$stmt->execute();
		$count = $stmt->rowCount();

		if($status==true){
			if($count>0){
				//echo "<script>alert('Thêm mới thành công');</script>";
				return true;
			}else{
				return false;

			}
		}else{
			return false;
		}
		
		


		
		
		
	}
	public static function addSupplierProccess(){
		
			
		$db = DB::getConnection();
			
		
		$sql = "INSERT INTO supplier SET supplier_name=:a ,address=:b ,phone=:c ,email=:d";

		//Bước 3: Tiến hành Prepare câu truy vấn
		$stmt = $db->prepare($sql);
		

		

		//Bước 4: Truyền giá trị cho các tham số trong câu truy vấn
		$stmt->bindParam(":a",$_POST['txtName']);
		$stmt->bindParam(":b",$_POST['txtAddress']);
		$stmt->bindParam(":c",$_POST['txtPhone']);
		
		$stmt->bindParam(":d",$_POST['txtWebsite']);
		
		//Bước 5: Thực thi câu truy vấn
		$stmt->execute();
		$count = $stmt->rowCount();

		
			if($count>0){
				//echo "<script>alert('Thêm mới thành công');</script>";
				return true;
			}else{
				return false;

			}
		
		
		


		
		
		
	}

	public static function UpdateSupplierProccess($id){
		
			
		$db = DB::getConnection();
			
		
		$sql = "UPDATE supplier SET supplier_name=:a ,address=:b ,phone=:c ,email=:d WHERE id_supplier=:e";

		//Bước 3: Tiến hành Prepare câu truy vấn
		$stmt = $db->prepare($sql);
		

		

		//Bước 4: Truyền giá trị cho các tham số trong câu truy vấn
		$stmt->bindParam(":a",$_POST['txtName']);
		$stmt->bindParam(":b",$_POST['txtAddress']);
		$stmt->bindParam(":c",$_POST['txtPhone']);
		
		$stmt->bindParam(":d",$_POST['txtWebsite']);
		$stmt->bindParam(":e",$id);
		
		//Bước 5: Thực thi câu truy vấn
		$stmt->execute();
		$count = $stmt->rowCount();

		
			if($count>0){
				//echo "<script>alert('Thêm mới thành công');</script>";
				return true;
			}else{
				return false;

			}
		
		
		


		
		
		
	}
	public static function deleteProduct($id){
		
				$db = DB::getConnection();
				
				
				$query = "DELETE FROM product WHERE id_product = ?";
				$stmt = $db->prepare($query);
				$stmt->bindParam(1,$id);

				
				
				if($stmt->execute()){
				
					return true;
				}else{
					return false;
				}
				
				

	}
	public static function deleteSupplier($id){
		
				$db = DB::getConnection();
				
				
				$query = "DELETE FROM supplier WHERE id_supplier = ?";
				$stmt = $db->prepare($query);
				$stmt->bindParam(1,$id);

				
				
				if($stmt->execute()){
				
					return true;
				}else{
					return false;
				}
				
				

	}
public static function getProductById($id){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM product WHERE id_product=:m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$id);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}
	public static function getSupplierById($id){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM supplier WHERE id_supplier=:m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$id);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

public static function updateProductProccess($id){
		
			
		$db = DB::getConnection();
			
		
		$sql = "UPDATE product SET name=:a ,price=:b ,sale=:c ,image=:d ,modem=:e ,id_role=:f, quantity=:g, id_gender=:h, Color=:i, size=:j, description=:k,id_supplier=:l,status=:m WHERE id_product=:n";

		//Bước 3: Tiến hành Prepare câu truy vấn
		$stmt = $db->prepare($sql);
		$statusproduct=1;
		$filesName 	= $_FILES['txtFile']['name']; 
		$filesType 	= $_FILES['txtFile']['type'];
		$filesTmp 	= $_FILES['txtFile']['tmp_name'];
		$filesError = $_FILES['txtFile']['error'];
		$filesSize	= $_FILES['txtFile']['size'];
		$status = false;//Biến trạng thái biểu diễn upload thành công hoặc thất bại

		for($i = 0; $i<count($filesName); $i++){
			if(($filesType[$i] == "image/jpeg" || $filesType[$i] == "image/png") && $filesSize[$i] <=20000000 && $filesError[$i] == 0){
				if((move_uploaded_file($filesTmp[$i], "C:/xampp1/htdocs/projectcuoikhoa/projectcuoikhoa/mvc/assets/images/".$filesName[$i]))==true){
					$status = true;
				}
			}
		}

		//Bước 4: Truyền giá trị cho các tham số trong câu truy vấn
		$stmt->bindParam(":a",$_POST['txtName']);
		$stmt->bindParam(":b",$_POST['txtPrice']);
		$stmt->bindParam(":c",$_POST['txtSale']);
		$stmt->bindParam(":d",implode(";", $filesName));
		$stmt->bindParam(":e",$_POST['txtModem']);
		$stmt->bindParam(":f",$_POST['cbRole']);
		$stmt->bindParam(":g",$_POST['txtQuantity']);
		$stmt->bindParam(":h",$_POST['cbGender']);
		$stmt->bindParam(":i",$_POST['txtColor']);
		$stmt->bindParam(":j",$_POST['txtSize']);
		$stmt->bindParam(":k",$_POST['txtDesc']);
		$stmt->bindParam(":l",$_POST['cbSupplier']);
		$stmt->bindParam(":m",$statusproduct);
		$stmt->bindParam(":n",$id);
		//Bước 5: Thực thi câu truy vấn
		$stmt->execute();
		$count = $stmt->rowCount();

		if($status==true){
			if($count>0){
				//echo "<script>alert('Thêm mới thành công');</script>";
				return true;
			}else{
				return false;

			}
		}else{
			return false;
		}
		
		


		
		
		
	}	














	public static function getAllSales(){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product ORDER BY sale DESC LIMIT 0,4";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}
	public static function getAllSold(){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product ORDER BY sold DESC LIMIT 0,4";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getProductByModem($id){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM product JOIN supplier ON product.id_supplier=supplier.id_supplier WHERE modem=:m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$id);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}



	public static function search($searchVal){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE name LIKE '%".$searchVal."%'";
		$stmt = $db->prepare($sql);
		//$stmt->bindParam(":n", $searchVal);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}


	public static function getProductByColor($color){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM product WHERE color like '%".$color."%'";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getProductByGender($gender){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE id_gender=:m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$gender);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getProductByPrice($min,$max){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE price BETWEEN :m and :n";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$min);
		$stmt->bindParam(":n",$max);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

		public static function getProductByGenderAndPrice($gender,$min,$max){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE id_gender = :l AND price BETWEEN :m and :n";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":l",$gender);
		$stmt->bindParam(":m",$min);
		$stmt->bindParam(":n",$max);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

		public static function getProductByGenderAndBrand($gender,$brand){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE id_gender = :l AND id_supplier = :m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":l",$gender);
		$stmt->bindParam(":m",$brand);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getProductByRoleAndBrand($role,$brand){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE id_role = :l AND id_supplier = :m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":l",$role);
		$stmt->bindParam(":m",$brand);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getProductByRoleAndPrice($gender,$min,$max){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE id_gender = :l AND price BETWEEN :m and :n";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":l",$role);
		$stmt->bindParam(":m",$min);
		$stmt->bindParam(":n",$max);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getProductByRole($role){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM product WHERE id_role = :m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$role);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}
	public static function getProductByModem1($id){
		$db = DB::getConnection();
		$sql = "SELECT * FROM product WHERE modem=:m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$id);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			return $stmt->fetch(PDO::FETCH_ASSOC);
		}
	}



}
?>











