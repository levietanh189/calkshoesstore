<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Trang quản lý</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />    
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        function xoa(id) {
            var result = window.confirm("Bạn có chắc muốn xóa sản phẩm có mã: "+id);

            if (result==true) {
                window.location.href="http://127.0.0.1/projectcuoikhoa/projectcuoikhoa/mvcadmin/?controller=product&action=deleteproduct&id="+id;
            }
        }
        function xoa1(id) {
            var result = window.confirm("Bạn có chắc muốn xóa nhà cung cấp có mã: "+id);

            if (result==true) {
                window.location.href="http://127.0.0.1/projectcuoikhoa/projectcuoikhoa/mvcadmin/?controller=product&action=deletesupplier&id="+id;
            }
        }
         function xoa2(id) {
            var result = window.confirm("Bạn có chắc muốn xóa nhân viên có mã: "+id);

            if (result==true) {
                window.location.href="http://127.0.0.1/projectcuoikhoa/projectcuoikhoa/mvcadmin/?controller=customer&action=deletecustomer&id="+id;
            }
        }
    </script>

</head>

<body>

<div class="wrapper">
    <?php 
if (isset($_SESSION['logged_in2']) && $_SESSION['logged_in2']==true) {
   
   


?>
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="<?php echo PATH;?>" class="simple-text">
                    Admin
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="<?php echo PATH;?>">
                        <i class="pe-7s-graph"></i>
                        <p>Thống kê</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo PATH;?>/?controller=product&action=allproduct">
                        <i class="pe-7s-note2"></i>
                        <p>Quản lý tin tức</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo PATH;?>/?controller=product&action=allsupplier">
                        <i class="pe-7s-note2"></i>
                        <p>Quản lý danh mục tin tức</p>
                    </a>
                </li>
                
                <li>
                    <a href="<?php echo PATH;?>/?controller=customer&action=allcustomer">
                        <i class="pe-7s-note2"></i>
                        <p>Quản lý người dùng</p>
                    </a>
                </li>
                
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Thống kê</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-globe"></i>
                                    <b class="caret hidden-sm hidden-xs"></b>
                                    <span class="notification hidden-sm hidden-xs">5</span>
                                    <p class="hidden-lg hidden-md">
                                        5 Thông báo
                                        <b class="caret"></b>
                                    </p>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Thông báo 1</a></li>
                                <li><a href="#">Thông báo 2</a></li>
                                <li><a href="#">Thông báo 3</a></li>
                                <li><a href="#">Thông báo 4</a></li>
                                
                              </ul>
                        </li>
                        
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                           <a href="<?php echo PATH;?>/?controller=customer&action=profile">
                               <p>Tài khoản</p>
                            </a>
                        </li>
                       
                        <li>
                            <a href="<?php echo PATH;?>/?controller=customer&action=logout">
                                <p>Đăng xuất</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg"></li>
                    </ul>
                </div>
            </div>
        </nav>
        <?php
        }else{
        
    }
        ?>

        <?=@ $content ?>
      
        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
                             Quản lý khách hàng
                            </a>
                        </li>
                       
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script> Made by Lê Việt Anh
                </p>
            </div>
        </footer>

    </div>
</div>


</body>


    <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/chartist.min.js"></script>
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>
    <script src="assets/js/validate.js" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function(){

            demo.initChartist();

            $.notify({
                icon: 'pe-7s-gift',
                message: "Xin chào bạn đến với trang quản trị"

            },{
                type: 'info',
                timer: 4000
            });

        });
    </script>

</html>

