 <div class="content">
            <div class="container-fluid">

              <?php

                    foreach ($data as $k => $v) {
                       
                 ?>
                <div class="row">
 
                
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Người dùng</h4>
                            </div>
                            <div class="content">
                                <form>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Công ty</label>
                                                <input type="text" class="form-control" disabled placeholder="Company" value="CALK SHOES STORE">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Địa chỉ Email</label>
                                                <input type="email" class="form-control" placeholder="Email" readonly="readonly" value="<?php echo $v['Email'];?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Họ</label>
                                                <input type="text" class="form-control" placeholder="Company"  readonly="readonly" value="<?php echo $v['LastName'];?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tên</label>
                                                <input type="text" class="form-control" placeholder="Last Name" readonly="readonly" value="<?php echo $v['FirstName'];?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Địa chỉ</label>
                                                <input type="text" class="form-control" placeholder="Home Address" readonly="readonly" value="<?php echo $v['Address'];?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Quê Quán</label>
                                                <input type="text" class="form-control" placeholder="Quê quán" readonly="readonly" value="<?php echo $v['Country'];?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Số điện thoại</label>
                                                <input type="text" class="form-control" placeholder="SĐT" readonly="readonly" value="<?php echo $v['Phone'];?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Số tài khoản ngân hàng</label>
                                                <input type="number" class="form-control" readonly="readonly" value="<?php echo $v['atm'];?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <label>Chức vụ</label>
                                                <input type="text" class="form-control" readonly="readonly" value="<?php echo $v['Title'];?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Mức lương</label>
                                                <input type="number" class="form-control" readonly="readonly" value="<?php echo $v['Salary'];?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Tóm tắt bản thân</label>
                                                <textarea rows="5" class="form-control" readonly="readonly" placeholder="Mô tả bản thân"><?php echo $v['description'];?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-user">
                            <div class="image">
                                <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>
                            </div>
                            <div class="content">
                                <div class="author">
                                     <a href="#">
                                    <img class="avatar border-gray" src="assets/img/faces/<?php echo $v['Photo'];?>" alt="..."/>

                                      <h4 class="title"><?php echo $v['LastName'];?> <?php echo $v['FirstName'];?><br />
                                         <small><?php echo $v['Email'];?></small>
                                      </h4>
                                    </a>
                                </div>
                                <p class="description text-center"> 
                                    <?php echo $v['description'];?>
                                </p>
                            </div>
                            <hr>
                            <div class="text-center">
                                <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                                <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                                <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>

                            </div>
                        </div>
                    </div>               


                </div> 
                <?php

                       
                       }

                                
                ?>

          
            </div>
        </div>