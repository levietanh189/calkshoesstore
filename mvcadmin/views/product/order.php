 <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Quản lý đơn hàng</h4>
                                <p class="category">Các chức năng quản lý đơn hàng</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                                             
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>Id đơn hàng</th>
                                        <th>Tên khách hàng</th>
                                        <th>Tổng tiền</th>
                                        <th>Phương thức thanh toán</th>
                                        <th>Thơi gian lập đơn</th>
                                        <th>Địa chỉ</th>
                                        <th>Tình trạng</th>
                                    </thead>
                                   
                                    <tbody>
                                        <?php
                                        foreach ($data as $k => $v) {
                                            
                                        
                                        ?>
                                        <tr>
                                            <td><?php echo $v['id_transaction'] ?></td>
                                            <td><?php echo $v['customer_name'] ?></td>
                                            <td><?php echo $v['total'] ?></td>
                                            <?php
                                            $payment = '';
                                            if ($v['id_payment']==1) {
                                                $payment='Thanh toán khi nhận';
                                            }elseif ($v['id_payment']==2) {
                                                $payment='Thanh toán qua thẻ';
                                            }
                                            ?>
                                            <td><?php echo $payment ?></td>
                                            <td><?php echo $v['time'] ?></td>
                                            <td><?php echo $v['address'] ?></td>
                                             <?php
                                            $status = '';
                                            if ($v['status']==0) {
                                                $status='Chưa giao';
                                            }elseif ($v['status']==1) {
                                                $status='Đang xử lý';
                                            }elseif ($v['status']==2) {
                                                $status='Đã giao';

                                            }
                                            ?>
                                            <td><?php echo $status?></td>
                                        </tr>
                                        <?php
                                    }
                                        ?>
                                       
                                    </tbody>
                                
                                </table>
                            
                          
                            </div>
                            








                        </div>
                    </div> 
                </div>
            </div>
        </div>  