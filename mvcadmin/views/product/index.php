 <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="header">
                                <h4 class="title">Tổng quát</h4>
                            
                            </div>
                            <div class="content">
                                <div class="table-full-width">
                                    <table class="table">
                                        <tbody>
                                            <?php
                                            $count=0;
                                            $total=0;
                                            foreach ($data as $k => $v) {
                                                $count++;
                                                $total+=$v['total'];
                                            }
                                            ?>
                                            <tr>
                                                <th>Số đơn đã bán</th>
                                                <th>Tổng số tiền đã bán</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php
                                                    echo $count;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        echo $total;
                                                    ?>$
                                                </td>
                                                <td class="alo">
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-history"></i> Updated 3 minutes ago
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Mặt hàng bán chạy nhất</h4>
                                <p class="category">24 Hours gần nhất</p>
                            </div>
                            <div class="content">
                                <div class="table-full-width">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th>Tên</th>
                                                <th>Giá</th>
                                                <th>Mô tả</th>
                                                <th>Số lượng trong kho</th>
                                                <th>Số lượng đã bán</th>
                                            </tr>
                                            <?php
                                            foreach ($data2 as $k => $v) {
                                                
                                            
                                            ?>
                                            <tr>
                                                <td>
                                                   <?php echo $v['name']?>
                                                </td>
                                                <td>
                                                    <?php echo $v['price']?>
                                                </td>
                                                <td>
                                                   <?php echo $v['description']?>
                                                </td>
                                                <td>
                                                   <?php echo $v['quantity']?>
                                                </td>
                                                <td>
                                                   <?php echo $v['sold']?>
                                                </td>

                                            </tr>
                                            <?php
                                        }
                                            ?>
                                            
                                        </tbody>
                                    </table>
                                </div>

                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-history"></i> Updated 3 minutes ago
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-6">
                        <div class="card ">
                            <div class="header">
                                <h4 class="title">Sales</h4>
                            </div>
                            <div class="content">
                                <div class="table-full-width">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th>Tên</th>
                                                <th>Giá</th>
                                                <th>Mô tả</th>
                                                <th>Sales</th>
                                            </tr>
                                            <?php
                                            foreach ($data1 as $k => $v) {
                                                
                                            
                                            ?>
                                            <tr>
                                                <td>
                                                   <?php echo $v['name']?>
                                                </td>
                                                <td>
                                                    <?php echo $v['price']?>
                                                </td>
                                                <td>
                                                   <?php echo $v['description']?>
                                                </td>
                                                <td>
                                                   <?php echo $v['sale']?>%
                                                </td>
                                               

                                            </tr>
                                            <?php
                                        }
                                            ?>
                                            
                                        </tbody>
                                    </table>
                                </div>

                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-history"></i> Updated 3 minutes ago
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card ">
                            <div class="header">
                                <h4 class="title">Nhiệm vụ</h4>
                            
                            </div>
                            <div class="content">
                                <div class="table-full-width">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td>
                          <div class="checkbox">
                                <input id="checkbox1" type="checkbox">
                                <label for="checkbox1"></label>
                              </div>
                                                </td>
                                                <td>Tăng trưởng doanh thu bán hàng lên 24%</td>
                                                <td class="td-actions text-right">
                                                    <button type="button" rel="tooltip" title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                          <div class="checkbox">
                                <input id="checkbox2" type="checkbox" checked>
                                <label for="checkbox2"></label>
                              </div>
                                                </td>
                                                <td>Hướng người tiêu dùng đến các mặt hàng giá thành cao</td>
                                                <td class="td-actions text-right">
                                                    <button type="button" rel="tooltip" title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                          <div class="checkbox">
                                <input id="checkbox3" type="checkbox">
                                <label for="checkbox3"></label>
                              </div>
                                                </td>
                                                <td>Gia tăng khả năng chốt đơn cho nhân viên
                        </td>
                                                <td class="td-actions text-right">
                                                    <button type="button" rel="tooltip" title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                          <div class="checkbox">
                                <input id="checkbox4" type="checkbox" checked>
                                <label for="checkbox4"></label>
                              </div>
                                                </td>
                                                <td>Khắc phục các sự cố trong quá trình giao hàng</td>
                                                <td class="td-actions text-right">
                                                    <button type="button" rel="tooltip" title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                          <div class="checkbox">
                                <input id="checkbox5" type="checkbox">
                                <label for="checkbox5"></label>
                              </div>
                                                </td>
                                                <td>Đẩy mạnh bán hàng online</td>
                                                <td class="td-actions text-right">
                                                    <button type="button" rel="tooltip" title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                          <div class="checkbox">
                                <input id="checkbox6" type="checkbox" checked>
                                <label for="checkbox6"></label>
                              </div>
                                                </td>
                                                <td>Nâng cấp hệ thống,cơ sở vật chất các cửa hàng</td>
                                                <td class="td-actions text-right">
                                                    <button type="button" rel="tooltip" title="Edit Task" class="btn btn-info btn-simple btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-history"></i> Updated 3 minutes ago
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>