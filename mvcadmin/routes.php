<?php
define("PATH", "http://127.0.0.1/mvcadmin");



$controllers = array(
	'pages'		=> ['home', 'error'],
	'product'	=> ['index','deleteproduct','updateproduct','order','allproduct','addproduct','addproductproccess','deleteproduct','updateproductproccess','categoriesbrand','allsupplier','addsupplier','addsupplierproccess','deletesupplier','updatesupplier','updatesupplierproccess'],
	'customer'	=> ['login','proccess','register','logout','profile','allcustomer','addcustomer','addcustomerproccess','deletecustomer','updatecustomer','updatecustomerproccess']
);

if(!array_key_exists($controller, $controllers) || !in_array($action, $controllers[$controller])){
	$controller = "pages";
	$action = "error";
}



include_once "controllers/" . $controller . "_" . "controller.php";
$klass = str_replace("_", "", ucwords($controller,'_')). "Controller";
$controller = new $klass;

$controller->$action();

?>







