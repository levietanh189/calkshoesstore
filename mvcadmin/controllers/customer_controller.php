<?php
/**
 * 
 */
require_once "models/customer.php";
require_once "models/product.php";
require_once "controllers/base_controller.php";

class CustomerController extends BaseController
{
	public function __construct(){
		$this->folder = "customer";
	}

	public function login(){
		$this->render("login");
	}

	public function proccess(){
		if (isset($_POST['btnLogin'])) {

			$email = $_POST['txtMail'];
			$pass = $_POST['txtPass'];
				
			$data = CustomerModel::checkLogin($email,$pass);
			$_SESSION['logged_in2'] = false;
			$_SESSION['confirm'] = false;
			if ($data==true) {
				$_SESSION['logged_in2'] = true;
				$_SESSION['customer'] = $email;
				 
				header("location:".PATH);
			}else{

				$_SESSION['confirm1'] = true;
				header("location:".PATH.'/?controller=customer&action=login');
			}
		}
	}

	public function logout(){
		session_unset($_SESSION['customer']);

		$_SESSION['logged_in2'] = false;
		header("location:".PATH);
		
	}
	
	public function profile(){
		$email = $_SESSION['customer'];

		$data = CustomerModel::getEmployeesByEmail($email);
		
		$this->render("profile",$data);
	}























	public function allcustomer(){
		$data = CustomerModel::getAllCustomer();
		
		$this->render("allcustomer",$data);
	}	
	public function addcustomer(){

		
		$this->render("addcustomer");
	}	
	
	public function addcustomerproccess(){
		if(isset($_POST['btnCreateCus'])){
		$data = CustomerModel::addCustomerProccess();
		if ($data==true) {
			$_SESSION['add2']=true;
			header("location:".PATH.'/?controller=customer&action=allcustomer');
		}else{
			$_SESSION['add2']=false;
			header("location:".PATH.'/?controller=customer&action=addcustomer');
			
		}
		
		}else{
			header("location:".PATH);
		}
	}

	public function deletecustomer(){

		if (isset($_GET['id']) && $_GET['id']!="") {
			$id=$_GET['id'];
			$data=CustomerModel::deleteCustomer($id);

			
			if ($data==true) {
			$_SESSION['delete2']=true;
			header("location:".PATH.'/?controller=customer&action=allcustomer');
		}else{
			$_SESSION['delete2']=false;
			header("location:".PATH.'/?controller=customer&action=allcustomer');
			
		}
		
		}else{
			header("location:".PATH);
		}

	}

	public function updatecustomer(){
		if (isset($_GET['id']) && $_GET['id']!="") {
			
		$id=$_GET['id'];
		$data = CustomerModel::getCustomerById($id);
		
		
		$this->render("updatecustomer",$data);}
		else{
			header("location:".PATH.'/?controller=customer&action=allcustomer');
		}
	}	
	
	public function updatecustomerproccess(){
		if(isset($_POST['btnUpdateCus'])){
			if (isset($_GET['id']) && $_GET['id']!="") {
				$id=$_GET['id'];
				$data = CustomerModel::updateCustomerProccess($id);
				if ($data==true) {
					$_SESSION['update2']=true;
					header("location:".PATH.'/?controller=customer&action=allcustomer');
				}else{
					$_SESSION['update2']=false;
					header("location:".PATH.'/?controller=customer&action=updatecustomer&id='.$id);
					
				}
		}else{
			header("location:".PATH.'/?controller=customer&action=addcustomer');
	}
		}else{
			header("location:".PATH);
		}
	}


	
}
?>