<?php
/**
 * 
 */
require_once "models/product.php";
require_once "models/customer.php";
require_once "controllers/base_controller.php";

class ProductController extends BaseController
{
	public function __construct(){
		$this->folder = "product";
	}

	public function index(){

		$data = ProductModel::getAllTransactionPay();
		$data1 = ProductModel::getAllSales();
		$data2 = ProductModel::getAllSold();
		$this->render("index",$data,$data1,$data2);
	}





	

	public function detail(){
		$id= isset($_GET['id']) ? $_GET['id'] : "";

		$data = ProductModel::getProductByModem($id);
		$data1 = ProductModel::getAllDate();
		
		
		$this->render("detail",$data,$data1);
	}

	public function search(){
		$searchVal = $_POST['txtSearch'];
		$data = ProductModel::search($searchVal);
		$this->render("result",$data);
	}

	public function categories(){
		
		
		$this->render("categories");
	}

	public function color(){
		$color = isset($_GET['color']) ? $_GET['color'] : "";
		$data = ProductModel::getProductByColor($color);
		$this->render("categories",$data);

	}

	public function gender(){
		$gender = isset($_GET['gender']) ? $_GET['gender'] : "";
		$data = ProductModel::getProductByGender($gender);
		$this->render("categories",$data);

	}

	public function price(){
		$min = isset($_GET['min']) ? $_GET['min'] : "";
		$max = isset($_GET['max']) ? $_GET['max'] : "";
		$data = ProductModel::getProductByPrice($min,$max);
		$this->render("categories",$data);
	}

	public function genderandprice(){
		$gender= isset($_GET['gender']) ? $_GET['gender'] : "";
		$min = isset($_GET['min']) ? $_GET['min'] : "";
		$max = isset($_GET['max']) ? $_GET['max'] : "";
		$data = ProductModel::getProductByGenderAndPrice($gender,$min,$max);
		$this->render("categories",$data);
	}
	public function genderandbrand(){
		$gender= isset($_GET['gender']) ? $_GET['gender'] : "";
		$brand=isset($_GET['brand']) ? $_GET['brand'] : "";
		$data = ProductModel::getProductByGenderAndBrand($gender,$brand);
		$this->render("categories",$data);
	}
	public function roleandprice(){
		$role= isset($_GET['role']) ? $_GET['role'] : "";
		$min = isset($_GET['min']) ? $_GET['min'] : "";
		$max = isset($_GET['max']) ? $_GET['max'] : "";
		$data = ProductModel::getProductByRolerAndPrice($role,$min,$max);
		$this->render("categories",$data);
	}
	public function role(){
		$role= isset($_GET['role']) ? $_GET['role'] : "";
		$data = ProductModel::getProductByRole($role);
		$this->render("categories",$data);
	}

	public function categoriesbrand(){
		
		$this->render("categories1");
	}







	public function order(){
		$data = CustomerModel::getAllOrder();
		
		$this->render("order",$data);
	}
	public function allproduct(){
		$data = ProductModel::getAllProduct();
		
		$this->render("allproduct",$data);
	}	
	public function addproduct(){

		$data = ProductModel::getAllRole();
		$data1 = ProductModel::getAllGender();
		$data2 = ProductModel::getAllSupplier();
		$this->render("addproduct",$data,$data1,$data2);
	}	
	
	public function addproductproccess(){
		if(isset($_POST['btnCreate'])){
		$data = ProductModel::addProductProccess();
		if ($data==true) {
			$_SESSION['add']=true;
			header("location:".PATH.'/?controller=product&action=allproduct');
		}else{
			$_SESSION['add']=false;
			header("location:".PATH.'/?controller=product&action=addproduct');
			
		}
		
		}else{
			header("location:".PATH);
		}
	}

	public function deleteproduct(){

		if (isset($_GET['id']) && $_GET['id']!="") {
			$id=$_GET['id'];
			$data=ProductModel::deleteProduct($id);

			
			if ($data==true) {
			$_SESSION['delete']=true;
			header("location:".PATH.'/?controller=product&action=allproduct');
		}else{
			$_SESSION['delete']=false;
			header("location:".PATH.'/?controller=product&action=allproduct');
			
		}
		
		}else{
			header("location:".PATH);
		}

	}

	public function updateproduct(){
		if (isset($_GET['id']) && $_GET['id']!="") {
			
		$id=$_GET['id'];
		$data = ProductModel::getAllRole();
		$data1 = ProductModel::getAllGender();
		$data2 = ProductModel::getAllSupplier();
		$data3 = ProductModel::getProductById($id);
		
		$this->render("updateproduct",$data,$data1,$data2,$data3);}
		else{
			header("location:".PATH.'/?controller=product&action=allproduct');
		}
	}	
	
	public function updateproductproccess(){
		if(isset($_POST['btnUpdate'])){
			if (isset($_GET['id']) && $_GET['id']!="") {
				$id=$_GET['id'];
				$data = ProductModel::updateProductProccess($id);
				if ($data==true) {
					$_SESSION['update']=true;
					header("location:".PATH.'/?controller=product&action=allproduct');
				}else{
					$_SESSION['update']=false;
					header("location:".PATH.'/?controller=product&action=updateproduct&id='.$id);
					
				}
		}else{
			header("location:".PATH.'/?controller=product&action=addproduct');
	}
		}else{
			header("location:".PATH);
		}
	}









	public function allsupplier(){
		$data = ProductModel::getAllSupplier();
		
		$this->render("allsupplier",$data);
	}	
	public function addsupplier(){

		
		$this->render("addsupplier");
	}	
	
	public function addsupplierproccess(){
		if(isset($_POST['btnCreateSup'])){
		$data = ProductModel::addSupplierProccess();
		if ($data==true) {
			$_SESSION['add1']=true;
			header("location:".PATH.'/?controller=product&action=allsupplier');
		}else{
			$_SESSION['add1']=false;
			header("location:".PATH.'/?controller=product&action=addsupplier');
			
		}
		
		}else{
			header("location:".PATH);
		}
	}

	public function deletesupplier(){

		if (isset($_GET['id']) && $_GET['id']!="") {
			$id=$_GET['id'];
			$data=ProductModel::deletesupplier($id);

			
			if ($data==true) {
			$_SESSION['delete1']=true;
			header("location:".PATH.'/?controller=product&action=allsupplier');
		}else{
			$_SESSION['delete1']=false;
			header("location:".PATH.'/?controller=product&action=allsupplier');
			
		}
		
		}else{
			header("location:".PATH);
		}

	}

	public function updatesupplier(){
		if (isset($_GET['id']) && $_GET['id']!="") {
			
		$id=$_GET['id'];
		$data = ProductModel::getSupplierById($id);
		
		
		$this->render("updatesupplier",$data);}
		else{
			header("location:".PATH.'/?controller=product&action=allsupplier');
		}
	}	
	
	public function updatesupplierproccess(){
		if(isset($_POST['btnUpdateSup'])){
			if (isset($_GET['id']) && $_GET['id']!="") {
				$id=$_GET['id'];
				$data = ProductModel::updateSupplierProccess($id);
				if ($data==true) {
					$_SESSION['update1']=true;
					header("location:".PATH.'/?controller=product&action=allsupplier');
				}else{
					$_SESSION['update1']=false;
					header("location:".PATH.'/?controller=product&action=updatesupplier&id='.$id);
					
				}
		}else{
			header("location:".PATH.'/?controller=product&action=addsupplier');
	}
		}else{
			header("location:".PATH);
		}
	}



}
?>











