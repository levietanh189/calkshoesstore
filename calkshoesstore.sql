-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th2 14, 2022 lúc 06:21 PM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `calkshoesstore`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id_category` int(11) NOT NULL,
  `name` char(60) COLLATE utf8_vietnamese_ci NOT NULL,
  `description` text COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id_category`, `name`, `description`) VALUES
(1, 'giầy lười', ''),
(2, 'sneaker', ''),
(4, 'Giày Thể Thao', ''),
(5, 'Giày đá bóng', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comment`
--

CREATE TABLE `comment` (
  `id_cmt` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `comment` text COLLATE utf8_vietnamese_ci NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL,
  `name` char(60) COLLATE utf8_vietnamese_ci NOT NULL,
  `birthday` char(15) COLLATE utf8_vietnamese_ci NOT NULL,
  `phone` char(11) COLLATE utf8_vietnamese_ci NOT NULL,
  `email` char(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `password` char(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `address` char(200) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id_customer`, `name`, `birthday`, `phone`, `email`, `password`, `address`) VALUES
(1, 'vanh', '18/9/1999', '0123456789', 'abc@gmail.com', 'vanh123', 'abc'),
(2, 'vanh1', '18/9/1999', '0987654321', 'abc1@gmail.com', 'vanh1', ''),
(13, 'vanh12', '', '0917091334', 'abc12@gmail.com', 'vanh123', ''),
(15, 'vanh12', '', '0917091334', 'abc111@gmail.com', 'vanh123', ''),
(16, 'vanh', '', '0917091334', 'abc1234@gmail.com', 'vanh123', ''),
(17, 'vanh12', '', '0917091334', 'cucucu1999200@gmail.com', 'vanh123', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `employees`
--

CREATE TABLE `employees` (
  `id_employee` int(11) NOT NULL,
  `LastName` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `FirstName` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `Title` varchar(30) COLLATE utf8_vietnamese_ci NOT NULL,
  `BirthDate` datetime NOT NULL,
  `Address` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `Country` varchar(30) COLLATE utf8_vietnamese_ci NOT NULL,
  `atm` int(30) NOT NULL,
  `Phone` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `Email` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `Password` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `Photo` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `Salary` int(11) NOT NULL,
  `Role` int(1) NOT NULL,
  `desc` text COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `employees`
--

INSERT INTO `employees` (`id_employee`, `LastName`, `FirstName`, `Title`, `BirthDate`, `Address`, `Country`, `atm`, `Phone`, `Email`, `Password`, `Photo`, `Salary`, `Role`, `desc`) VALUES
(1, 'Nguyễn ', 'Minh Trường', 'Nhân viên bán hàng', '0000-00-00 00:00:00', 'Cổ Nhuế - Bắc Từ Liêm', 'Hà Nội', 123456789, '0917091334', 'abc123@gmail.com', 'vanh123', 'face-2.jpg', 1200, 1, 'Có trách nghiệm và tinh thần làm việc cao');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `feedback`
--

CREATE TABLE `feedback` (
  `id_feedback` int(11) NOT NULL,
  `name` char(60) COLLATE utf8_vietnamese_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `email` char(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `address` char(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `description` text COLLATE utf8_vietnamese_ci NOT NULL,
  `id_customer` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `gender`
--

CREATE TABLE `gender` (
  `id_gender` int(11) NOT NULL,
  `gender_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `gender`
--

INSERT INTO `gender` (`id_gender`, `gender_name`) VALUES
(1, 'Nam'),
(2, 'Nữ'),
(3, 'Trẻ em');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order`
--

CREATE TABLE `order` (
  `id_order` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `id_shipers` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id_role` int(11) NOT NULL,
  `role_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `payment`
--

CREATE TABLE `payment` (
  `id_payment` int(150) NOT NULL,
  `payment` varchar(150) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `payment`
--

INSERT INTO `payment` (`id_payment`, `payment`) VALUES
(1, 'Pay on delivery'),
(2, 'ATM');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id_product` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `price` float NOT NULL,
  `sale` float NOT NULL,
  `image` varchar(150) COLLATE utf8_vietnamese_ci NOT NULL,
  `description` text COLLATE utf8_vietnamese_ci NOT NULL,
  `status` bit(1) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `time_in` datetime NOT NULL,
  `modem` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `sold` int(150) NOT NULL,
  `size` int(40) NOT NULL,
  `id_role` int(11) DEFAULT NULL,
  `id_gender` int(11) DEFAULT NULL,
  `Color` varchar(150) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id_product`, `name`, `price`, `sale`, `image`, `description`, `status`, `id_supplier`, `id_category`, `time_in`, `modem`, `quantity`, `sold`, `size`, `id_role`, `id_gender`, `Color`) VALUES
(1, 'nike1', 250, 10, 'shoes-1.png', 'Nike đời đầu ', b'1', 1, 1, '2019-04-17 06:18:17', 'ele123', 123, 12, 0, 2, 2, 'xanh'),
(2, 'adidas 1', 150, 10, 'shoes-2.png', 'adidas  đời đầu', b'1', 2, 2, '2019-04-18 00:00:00', 'ele124', 50, 13, 0, 3, 1, 'đen'),
(3, 'thuongdinh1', 100, 10, 'shoes-3.png', 'Giày tốt giá tốt', b'0', 3, 1, '2019-04-11 04:00:00', 'ele115', 40, 14, 0, 2, 1, 'vàng'),
(4, 'Vans1', 500, 10, 'shoes-13.png', 'Giày được làm bằng 100% sợi nilon tổng hợp.GIữ được màu sắc dưới mọi điều kiện thời tiết', b'1', 4, 2, '2019-04-11 00:21:00', 'ele116', 20, 0, 0, 1, 1, 'Trắng'),
(5, 'Nike XS Max Plus', 700, 50, 'shoes-1.png', '', b'1', 1, 2, '2019-05-20 10:20:17', 'ELE1213', 20, 14, 0, 1, 3, 'xanh'),
(7, 'Thượng Đình Vintages', 450, 45, 'shoes-2.png', '', b'1', 1, 4, '2019-05-20 10:20:17', 'ELE1214', 22, 15, 0, 3, 2, 'đen');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `role_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `role`
--

INSERT INTO `role` (`id_role`, `role_name`) VALUES
(0, 'Sneaker'),
(1, 'Giày đá bóng'),
(2, 'Giày chạy'),
(3, 'Giày bóng chuyền');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `shipers`
--

CREATE TABLE `shipers` (
  `id_shipers` int(11) NOT NULL,
  `copanyName` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `Phone` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `shipers`
--

INSERT INTO `shipers` (`id_shipers`, `copanyName`, `Phone`) VALUES
(198, 'viettel post', '198'),
(199, 'ongvang', '199');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL,
  `supplier_name` char(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `address` char(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `phone` char(11) COLLATE utf8_vietnamese_ci NOT NULL,
  `email` char(50) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `supplier_name`, `address`, `phone`, `email`) VALUES
(1, 'nike', 'vietnam', '113', 'nike.com.vn'),
(2, 'adidas', 'newyork', '123', 'adidas.com'),
(3, 'Thượng Đình', '', '111', 'thuongdinh.com.vn'),
(4, 'Vans', '', '100', 'vans.com.vn');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `transactionpay`
--

CREATE TABLE `transactionpay` (
  `id_transaction` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `customer_name` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `id_payment` int(150) NOT NULL,
  `email` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `total` int(11) NOT NULL,
  `time` datetime NOT NULL DEFAULT current_timestamp(),
  `atm` int(11) NOT NULL,
  `cart_info` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `transactionpay`
--

INSERT INTO `transactionpay` (`id_transaction`, `id_customer`, `phone`, `customer_name`, `id_payment`, `email`, `address`, `total`, `time`, `atm`, `cart_info`, `status`) VALUES
(5, 17, 917091334, 'vanh12', 1, 'cucucu1999200@gmail.com', 'hai bà trưng hà nội', 350, '2019-06-05 00:49:42', 111, '', 0),
(6, 17, 917091334, 'vanh12', 1, 'cucucu1999200@gmail.com', 'hai bà trưng hà nội', 1125, '2019-06-05 00:51:08', 123, '', 1),
(7, 17, 917091334, 'vanh12', 1, 'cucucu1999200@gmail.com', 'hai bà trưng hà nội', 1050, '2019-06-05 23:40:35', 1236548799, '', 2),
(8, 1, 123456789, 'vanh', 1, 'abc@gmail.com', 'abc', 135, '2021-05-25 02:07:48', 2147483647, '', 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id_category`);

--
-- Chỉ mục cho bảng `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id_cmt`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Chỉ mục cho bảng `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id_employee`);

--
-- Chỉ mục cho bảng `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id_feedback`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Chỉ mục cho bảng `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id_gender`);

--
-- Chỉ mục cho bảng `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_employee` (`id_employee`),
  ADD KEY `id_shipers` (`id_shipers`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_role`);

--
-- Chỉ mục cho bảng `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id_payment`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id_product`),
  ADD UNIQUE KEY `modem` (`modem`),
  ADD KEY `id_supllier` (`id_supplier`),
  ADD KEY `id_category` (`id_category`),
  ADD KEY `id_role` (`id_role`),
  ADD KEY `id_gender` (`id_gender`);

--
-- Chỉ mục cho bảng `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Chỉ mục cho bảng `shipers`
--
ALTER TABLE `shipers`
  ADD PRIMARY KEY (`id_shipers`);

--
-- Chỉ mục cho bảng `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Chỉ mục cho bảng `transactionpay`
--
ALTER TABLE `transactionpay`
  ADD PRIMARY KEY (`id_transaction`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `FK_transaction` (`id_payment`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `comment`
--
ALTER TABLE `comment`
  MODIFY `id_cmt` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `employees`
--
ALTER TABLE `employees`
  MODIFY `id_employee` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id_feedback` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `order`
--
ALTER TABLE `order`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `payment`
--
ALTER TABLE `payment`
  MODIFY `id_payment` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `shipers`
--
ALTER TABLE `shipers`
  MODIFY `id_shipers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT cho bảng `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `transactionpay`
--
ALTER TABLE `transactionpay`
  MODIFY `id_transaction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`),
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`);

--
-- Các ràng buộc cho bảng `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`);

--
-- Các ràng buộc cho bảng `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`),
  ADD CONSTRAINT `order_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`),
  ADD CONSTRAINT `order_ibfk_3` FOREIGN KEY (`id_employee`) REFERENCES `employees` (`id_employee`),
  ADD CONSTRAINT `order_ibfk_4` FOREIGN KEY (`id_shipers`) REFERENCES `shipers` (`id_shipers`);

--
-- Các ràng buộc cho bảng `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`id_supplier`) REFERENCES `supplier` (`id_supplier`),
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`id_category`) REFERENCES `category` (`id_category`),
  ADD CONSTRAINT `product_ibfk_3` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`),
  ADD CONSTRAINT `product_ibfk_4` FOREIGN KEY (`id_gender`) REFERENCES `gender` (`id_gender`);

--
-- Các ràng buộc cho bảng `transactionpay`
--
ALTER TABLE `transactionpay`
  ADD CONSTRAINT `FK_transaction` FOREIGN KEY (`id_payment`) REFERENCES `payment` (`id_payment`),
  ADD CONSTRAINT `transactionpay_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
