 <div class="breadcrumbs">
			<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>/
					<li class="active">Giày theo hãng:</li>
				</ol>
			</div>
			</div>
		</div>

		<div class="product">
			<div class="container">
				<div class="single-main row">
					<div class="col-md-9 single-main-left">
						

						<div class="products">
							<div class="product-one row">
			                    <div class="col-md-3 col-sm-3 col-xs-3 brand-item-cover">
					                <div class="brand-item" style="background-image:url('assets/images/brand1.jpg'); "></div>
					                </div>
					                <div class="col-md-3 col-sm-3 col-xs-3 brand-item-cover">
					                    <div class="brand-item" style="background-image:url('assets/images/brand2.jpg'); "></div>
					                </div>
					                <div class="col-md-3 col-sm-3 col-xs-3  brand-item-cover">
					                    <div class="brand-item" style="background-image:url('assets/images/brand3.jpg'); "></div>
					                </div>
					                <div class="col-md-3 col-sm-3 col-xs-3 brand-item-cover">
					                    <div class="brand-item" style="background-image:url('assets/images/brand4.jpg'); "></div>
					                </div>
					                <div class="col-md-3 col-sm-3 col-xs-3 brand-item-cover">
					                    <div class="brand-item" style="background-image:url('assets/images/brand5.jpg'); "></div>
					                </div>
					                <div class="col-md-3 col-sm-3 col-xs-3 brand-item-cover">
					                    <div class="brand-item" style="background-image:url('assets/images/brand6.jpg'); "></div>
					                </div>
			                      
			                      </div>
						
						</div>
					</div>



					<div class="col-md-3 single-right">
								<h3>Loại</h3>
								<ul class="product-categories">
									<li><a href="<?php echo PATH;?>/?controller=product&action=gender&gender=1">Giày cho nam</a> <span class="count">(14)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=gender&gender=2">Giày cho nữ</a> <span class="count">(2)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=gender&gender=3">Giày trẻ em</a> <span class="count">(2)</span></li>
									
								</ul>
								<h3>Colors</h3>
								<ul class="product-categories">
									<li><a href="<?php echo PATH;?>/?controller=product&action=color&color=xanh">Xanh</a> <span class="count">(14)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=color&color=vàng">Vàng</a> <span class="count">(2)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=color&color=trắng">Trắng</a> <span class="count">(2)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=color&color=đen">Đen</a> <span class="count">(8)</span></li>
									
								</ul>
								<h3>Price</h3>
								<ul class="product-categories p1">
									<li><a href="<?php echo PATH;?>/?controller=product&action=price&min=0&max=200">Dưới 200$</a> <span class="count">(14)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=price&min=200&max=500">200$->500$</a> <span class="count">(2)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=price&min=500&max=100000">Trên 500$</a> <span class="count">(2)</span></li>
									
								</ul>
					</div>
					<div class="clearfix"> </div>

				</div>
			</div>
		</div>	