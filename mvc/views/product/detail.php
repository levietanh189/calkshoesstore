	    <div class="breadcrumbs">
			<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>/
					<li class="active">Single</li>
				</ol>
			</div>
			</div>
		</div>

		<div class="single contact">
			<div class="container">
				<div class="single-main row">
					<div class="col-md-9 single-main-left">
						<div class="sngl-top row">
							<?php
								foreach ($data as $k => $v) {
								
								
								?>
							<div class="col-md-5 single-top-left">	
								<div class="flexslider">
									<ul class="slides">
										<li data-thumb="assets/images/<?php echo $v['image'];?>">
											<img src="assets/images/<?php echo $v['image'];?>" />
										</li>
										
									</ul>
								</div>
									<!-- FlexSlider -->
								  	<script defer src="assets/js/jquery.flexslider.js"></script>
									<link rel="stylesheet" href="assets/css/flexslider.css" type="text/css" media="screen" />

									<script>
										// Can also be used with $(document).ready()
										$(window).load(function() {
										  $('.flexslider').flexslider({
										    animation: "slide",
										    controlNav: "thumbnails"
										  });
										});
									</script>
							</div>	
							<div class="col-md-7 single-top-right">
								
								<div class="details-left-info simpleCart_shelfItem">
										<h3><?php echo $v['name'];?></h3>
										<?php
											if ($v['status']==1) {
												$status='Còn hàng';
											}else{
												$status='Hết hàng';
											}
										?>
										<p class="availability">Tình trạng: <span class="color"><?php echo $status;?></span></p>
										<div class="price_single">
											<span class="reducedfrom"><?php echo $v['price'];?>$</span>
											<span class="actual item_price"><?php echo ($v['price']-($v['price']*($v['sale']/100)));?>$</span>
										</div>
										<h2 class="quick">Nhãn hiệu:</h2>
										<p class=""><?php echo $v['supplier_name'];?></p>
										<?php
											if ($v['id_gender']==1) {
												$gender='Nam';
											}else if ($v['id_gender']==2) {
												$gender='Nữ';
											}else{
												$gender='Trẻ em';
											}
										?>
										<h2 class="quick">Giới tính:</h2>
										<p class=""><?php echo $gender?></p>
										<?php
											if ($v['id_role']==1) {
												$role='Giày đá bóng';
											}else if ($v['id_role']==2) {
												$role='Giày chạy';
											}else{
												$role='Giày bóng chuyền';
											}
										?>

										<h2 class="quick">Loại:</h2>
										<p class=""><?php echo $role;?></p>
										
										<form action="<?php echo PATH;?>/?controller=cart&action=add&modem=<?php echo $v['modem'];?>" method="post">
										<div class="quantity_box">
											<ul class="product-qty">
												<span>Kích cỡ:</span>
												<select name="slSize">
													<option>15</option>
													<option>16</option>
													<option>17</option>
													<option>18</option>
													<option>19</option>
													<option>20</option>
													<option>21</option>
													<option>22</option>
													<option>23</option>
													<option>24</option>
													<option>25</option>
													<option>26</option>
													<option>27</option>
													<option>28</option>
													<option>29</option>
													<option selected="selected">30</option>
													<option>31</option>
													<option>32</option>
													<option>33</option>
													<option>34</option>
													<option>35</option>
													<option>36</option>
													<option>37</option>
												</select>
											</ul>
										</div>
										<div class="quantity_box">
											<ul class="product-qty">
												<span>Số lượng:</span>
												<select name="slQuantity">
													<option selected="selected">1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
													<option>6</option>
												</select>
											</ul>
										</div>
										
									<div class="clearfix"> </div>

									<div class="single-but item_add">
									<input type="submit" value="add to cart"/>
									</div>
								</form>
								</div>
								
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="row" style="margin-top: 10%;">
					<div class="col-xs-12">
						<div style="font-size: 2rem;">Mô tả chi tiết</div>

						<div>
							<?php echo $v['description'];?>
						</div>
						<?php
									}
								?>
					</div>
				</div>
					</div>



					<div class="col-md-3 single-right">
								<h3>Loại</h3>
								<ul class="product-categories">
									<li><a href="<?php echo PATH;?>/?controller=product&action=gender&gender=1">Giày cho nam</a> <span class="count">(14)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=gender&gender=2">Giày cho nữ</a> <span class="count">(2)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=gender&gender=3">Giày trẻ em</a> <span class="count">(2)</span></li>
									
								</ul>
								<h3>Colors</h3>
								<ul class="product-categories">
									<li><a href="<?php echo PATH;?>/?controller=product&action=color&color=xanh">Xanh</a> <span class="count">(14)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=color&color=vàng">Vàng</a> <span class="count">(2)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=color&color=trắng">Trắng</a> <span class="count">(2)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=color&color=đen">Đen</a> <span class="count">(8)</span></li>
									
								</ul>
								<h3>Price</h3>
								<ul class="product-categories p1">
									<li><a href="<?php echo PATH;?>/?controller=product&action=price&min=0&max=200">Dưới 200$</a> <span class="count">(14)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=price&min=200&max=500">200$->500$</a> <span class="count">(2)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=price&min=500&max=100000">Trên 500$</a> <span class="count">(2)</span></li>
									
								</ul>
					</div>
					<div class="clearfix"> </div>

				</div>
			</div>
		</div>	