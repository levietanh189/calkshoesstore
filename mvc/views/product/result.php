 <div class="breadcrumbs">
			<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>/
					<li class="active">Kết quả tìm kiếm của bạn:</li>
				</ol>
			</div>
			</div>
		</div>

		<div class="product">
			<div class="container">
				<div class="single-main row">
					<div class="col-md-9 single-main-left">
						

						<div class="products">
							<div class="product-one row">
			                    <?php
			                       foreach ($data as $k => $v) {
			                    ?>
			                      <div class="col-md-3 col-xs-3 col-sm-3 product-left"> 
			                          <div class="p-one simpleCart_shelfItem"> 
			                                  <div class="saleproduct">-<?php echo $v['sale'];?>%</div>                           
			                                  <a href="<?php echo PATH;?>/?controller=product&action=detail&id=<?php echo $v['modem'];?>">
			                                      <img src="assets/images/<?php echo $v['image'];?>" alt="" />

			                                      <div class="mask">
			                                          <span>Quick View</span>
			                                      </div>
			                                  </a>
			                              <h4><?php echo $v['name'];?></h4>
			                              <p><a class="item_add" href="#"><i></i><span class=" item_price"><strike><?php echo $v['price'];?>$</strike></span></a></p>
			                              <p>-</p>
			                              <p><a class="item_add" href="#"><i></i> <span class=" item_price"><?php echo ($v['price']-($v['price']*($v['sale']/100)));?>$</span></a></p>
			                          
			                          </div>
			                      </div>
			                    <?php
			                      }
			                    ?>  
			                      
			                      </div>
						
						</div>
					</div>



					<div class="col-md-3 single-right">
								<h3>Loại</h3>
								<ul class="product-categories">
									<li><a href="<?php echo PATH;?>/?controller=product&action=gender&gender=1">Giày cho nam</a> <span class="count">(14)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=gender&gender=2">Giày cho nữ</a> <span class="count">(2)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=gender&gender=3">Giày trẻ em</a> <span class="count">(2)</span></li>
									
								</ul>
								<h3>Colors</h3>
								<ul class="product-categories">
									<li><a href="<?php echo PATH;?>/?controller=product&action=color&color=xanh">Xanh</a> <span class="count">(14)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=color&color=vàng">Vàng</a> <span class="count">(2)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=color&color=trắng">Trắng</a> <span class="count">(2)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=color&color=đen">Đen</a> <span class="count">(8)</span></li>
									
								</ul>
								<h3>Price</h3>
								<ul class="product-categories p1">
									<li><a href="<?php echo PATH;?>/?controller=product&action=price&min=0&max=200">Dưới 200$</a> <span class="count">(14)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=price&min=200&max=500">200$->500$</a> <span class="count">(2)</span></li>
									<li><a href="<?php echo PATH;?>/?controller=product&action=price&min=500&max=100000">Trên 500$</a> <span class="count">(2)</span></li>
									
								</ul>
					</div>
					<div class="clearfix"> </div>

				</div>
			</div>
		</div>	