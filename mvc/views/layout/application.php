<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>CalkShoesStore</title>

  
    
   
    <link rel="stylesheet" type="text/css" href="assets/css/calkstore.css">

    <!--bootstrap-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link href="assets/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
    <!--fontawesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <!-- Custom Theme files -->
    <!--theme-style-->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />  
    

    

</head>
<body>
   <div class="container-fluid">
    <div class="header" id="myHeader">
         <header  style="background: url('assets/images/background-header.jpg'); ">
            <!--=================== Header Top Section ===================-->
            <section class="header-top">
                  <div class="container">
                      <div class="row">
                          <div class="col-md-3 col-sm-3 col-xs-3 logo">
                              <h2><a href="<?php echo PATH;?>">CALK</a></h2>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-6 header-search">
                              
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-3 action">
                              <ul class="list-inline">
                                  
                                      <?php 
                                        if (isset($_SESSION['logged_in'])&& $_SESSION['logged_in']==true) {
                                          echo "<li>";
                                          echo "Xin chào ".$_SESSION['customer'];
                                          echo "</li>";

                                          echo "<li>";
                                          echo "<a href='".PATH."/?controller=customer&action=logout'><i class='fas fa-sign-in-alt'></i><span class='action-detail'> Logout</span></a>";
                                          echo "</li>";


                                          echo "<li>";
                                          echo "<a href='".PATH."/?controller=cart&action=cart' class='padding-bottom-10'><i class='fa fa-shopping-cart'></i> <span class='paira-cart-total-price'><span class='cart-total'>". $_SESSION['count']."</span></span></a>";
                                          echo "</li>";
                                        }else{
                                      ?>
                                      <li>
                                      <a href="<?php echo PATH;?>/?controller=customer&action=login"><i class="fas fa-sign-in-alt"></i><span class="action-detail"> Login</span></a>
                                    </li>
                                    <li><a href="<?php echo PATH;?>/?controller=customer&action=register"><i class="fas fa-user-plus"></i><span class="action-detail"> Register</span></a></li>
                                   
                                      <?php
                                    }
                                      ?>
                                  
                              </ul>
                              
                          </div>
                      </div>
                  </div>
              </section>
              <!--=================== Header Bottom Section ===================-->
              <section class="header-bottom">
                  <div class="container">
                      <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12" >
                              <!--=================== Main Menu ===================-->
                              <nav class="navbar-expand-lg navbar-light">
                                
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                  <span class="navbar-toggler-icon"></span>
                                </button>

                                <div class="collapse navbar-collapse navbar-header" id="navbarSupportedContent">
                                  <ul class="navbar-nav mr-auto">
                                    
                                     <li class="nav-item dropdown">
                                          <a class="nav-link" href="<?php echo PATH;?>/?controller=product&action=gender&gender=1" id="navbarDropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                                            Giày Nam
                                          </a>
                                          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <p><b>Theo giá:</b></p> 
                                            <a class="dropdown-item inline" href="<?php echo PATH;?>/?controller=product&action=genderandprice&gender=1&min=0&max=200"><i>Dưới 200$</i></a>
                                            <a class="dropdown-item inline" href="<?php echo PATH;?>/?controller=product&action=genderandprice&gender=1&min=200&max=500"><i>200$->500$</i></a>
                                            <a class="dropdown-item inline" href="<?php echo PATH;?>/?controller=product&action=genderandprice&gender=1&min=500&max=1000000"><i>Trên 500$</i></a>
                                            <p><b>Theo hãng</b></p>
                                             <?php
                                              $db = DB::getConnection();
                                              $sql = "SELECT * FROM supplier";
                                              $stmt = $db->prepare($sql);
                                              $stmt->execute();
                                              $count = $stmt->rowCount();
                                              if($count>0){
                                                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                                  echo "<a class='dropdown-item inline' href='".PATH."/?controller=product&action=genderandbrand&gender=1&brand=".$row['id_supplier']."'><i>".$row['supplier_name']."</i></a>";
                                                }
                                              }

                                            ?>
                                          </div>
                                      </li>
                                      <li class="nav-item dropdown">
                                          <a class="nav-link" href="<?php echo PATH;?>/?controller=product&action=gender&gender=2" id="navbarDropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                                            Giày Nữ
                                          </a>
                                          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <a class="dropdown-item inline" href="<?php echo PATH;?>/?controller=product&action=genderandprice&gender=2&min=0&max=200"><i>Dưới 200$</i></a>
                                            <a class="dropdown-item inline" href="<?php echo PATH;?>/?controller=product&action=genderandprice&gender=2&min=200&max=500"><i>200$->500$</i></a>
                                            <a class="dropdown-item inline" href="<?php echo PATH;?>/?controller=product&action=genderandprice&gender=2&min=500&max=1000000"><i>Trên 500$</i></a>
                                            <p><b>Theo hãng</b></p>
                                            <?php
                                              $db = DB::getConnection();
                                              $sql = "SELECT * FROM supplier";
                                              $stmt = $db->prepare($sql);
                                              $stmt->execute();
                                              $count = $stmt->rowCount();
                                              if($count>0){
                                                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                                  echo "<a class='dropdown-item inline' href='".PATH."/?controller=product&action=genderandbrand&gender=2&brand=".$row['id_supplier']."'><i>".$row['supplier_name']."</i></a>";
                                                }
                                              }

                                            ?>
                                          </div>
                                      </li>
                                       <li class="nav-item dropdown">
                                          <a class="nav-link" href="<?php echo PATH;?>/?controller=product&action=gender&gender=2" id="navbarDropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                                            Sneaker
                                          </a>
                                          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <a class="dropdown-item inline" href="<?php echo PATH;?>/?controller=product&action=sneakerandprice&min=0&max=200"><i>Dưới 200$</i></a>
                                            <a class="dropdown-item inline" href="<?php echo PATH;?>/?controller=product&action=sneakerandprice&min=200&max=500"><i>200$->500$</i></a>
                                            <a class="dropdown-item inline" href="<?php echo PATH;?>/?controller=product&action=sneakerandprice&min=500&max=1000000"><i>Trên 500$</i></a>
                                            
                                          
                                          </div>
                                      </li>
                                      <li class="nav-item dropdown">
                                          <a class="nav-link" href="<?php echo PATH;?>/?controller=product&action=gender&gender=3" id="navbarDropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                                            Giày Trẻ Em
                                          </a>
                                          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            <p><b>Theo giá:</b></p> 
                                            <a class="dropdown-item inline" href="<?php echo PATH;?>/?controller=product&action=genderandprice&gender=3&min=0&max=200"><i>Dưới 200$</i></a>
                                            <a class="dropdown-item inline" href="<?php echo PATH;?>/?controller=product&action=genderandprice&gender=3&min=200&max=500"><i>200$->500$</i></a>
                                            <a class="dropdown-item inline" href="<?php echo PATH;?>/?controller=product&action=genderandprice&gender=3&min=500&max=1000000"><i>Trên 500$</i></a>
                                            <p><b>Theo hãng</b></p>
                                             <?php
                                              $db = DB::getConnection();
                                              $sql = "SELECT * FROM supplier";
                                              $stmt = $db->prepare($sql);
                                              $stmt->execute();
                                              $count = $stmt->rowCount();
                                              if($count>0){
                                                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                                  echo "<a class='dropdown-item inline' href='".PATH."/?controller=product&action=genderandbrand&gender=3&brand=".$row['id_supplier']."'><i>".$row['supplier_name']."</i></a>";
                                                }
                                              }

                                            ?>
                                          </div>
                                      </li>
                                      <li class="nav-item dropdown">
                                          <a class="nav-link" href="<?php echo PATH;?>/?controller=product&action=gender&gender=1" id="navbarDropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                                            Thể thao
                                          </a>
                                          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                            
                                            <p><b>Theo môn</b></p>
                                             <?php
                                              $db = DB::getConnection();
                                              $sql = "SELECT * FROM role";
                                              $stmt = $db->prepare($sql);
                                              $stmt->execute();
                                              $count = $stmt->rowCount();
                                              if($count>0){
                                                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                                  echo "<a class='dropdown-item inline' href='".PATH."/?controller=product&action=role&role=".$row['id_role']."'><i>".$row['role_name']."</i></a>";
                                                 
                                                }


                                              }


                                            ?>
                                          </div>
                                      </li>
                                      <li class="nav-item dropdown">
                                          <a class="nav-link" href="<?php echo PATH;?>/?controller=product&action=categoriesbrand" id="navbarDropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                                            Nhãn hiệu
                                          </a>
                                          
                                      </li>
                                    
                                  </ul>
                                 
                                </div>
                              </nav>
                             
                          </div>
                      </div>
                  </div>
              </section>
            
        </header>
    </div>


	<?=@ $content ?>


    <div class="footer" style="background: url('assets/images/background-header.jpg');">
     <footer style="background-color: rgba(60,193,193,0.6); ">

        <!--=================== Footer Top Section ===================-->
        <section class="footer-top">
            <div class="container">
                <div class="row">
                    <!--=================== Footer Top Column 4 ===================-->
                    <div class="col-md-4 col-sm-6 col-xs-12 newsletter">
                        <!--=================== Newsletter ===================-->
                        <h4 class="paira-gap-3 margin-clear margin-bottom-20 text-uppercase">Cập nhật tin tức</h4>
                        <form class="form-inline margin-top-20 margin-bottom-20" action="" method="post" target="_blank" novalidate="" name="mc-embedded-subscribe-form" id="mc-embedded-subscribe-form">
                            <div class="form-group">
                                <input type="email" class="form-control email" name="newsletter-email" id="newsletter-email" placeholder="Email Address" required>
                            </div>
                            <button type="submit" class="btn btn-default"><i class="fa fa-angle-right"></i></button>
                        </form>
                        <!--=================== Social Icons Footer ===================-->
                        <ul class="list-inline link">

                            <li class="link-social" data-toggle="tooltip" title="Facebook">
                                <a href="https://www.facebook.com/" target="_blank">
                                    <span><i class="fab fa-facebook-square"></i></span>
                                </a>
                            </li>
                            <li class="link-social" data-toggle="tooltip" data-placement="top" title="Twitter">
                                <a href="https://twitter.com/" target="_blank">
                                    <span><i class="fab fa-twitter-square"></i></span>
                                </a>
                            </li>
                            <li class="link-social" data-toggle="tooltip" data-placement="top" title="LinkedIn">
                                <a href="https://www.linkedin.com/" target="_blank">
                                    <span><i class="fab fa-linkedin"></i></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--=================== Footer Top Column 1 ===================-->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <!--=================== Footer Middle Column 1 ===================-->
                        <h4 class="paira-gap-3 margin-clear margin-bottom-20 text-uppercase">About</h4>
                        <p class="text-justify">We just don't build your website we build your business.</p>
                        <p class="text-justify"><br>(88)-0000 0000 <br>support@abc.com</p>
                    </div>
                    <!--=================== Footer Top Column 2 ===================-->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <h4 class="paira-gap-3 margin-clear margin-bottom-20 text-uppercase">Link hỗ trợ</h4>
                        <ul class="list-unstyled footer-list-style">
                            <li><a href="about-us.php">Terms & Conditions</a></li>
                            <li><a href="about-us.php">Privacy Policy</a></li>
                            <li><a href="about-us.php">Return Policy</a></li>
                        </ul>
                    </div>
                    <!--=================== Footer Top Column 3 ===================-->
                    <div class="col-md-2 col-sm-6 col-xs-12 paira-gap-4">
                        <h4 class="paira-gap-3 margin-clear margin-bottom-20 text-uppercase">Thông tin</h4>
                        <ul class="list-unstyled footer-list-style">
                            <li><a href="about-us.php">About us</a></li>
                            <li><a href="single-blog.php">News</a></li>
                            <li><a href="contact-us.php">Contact us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!--=================== Footer Bottom Section ===================-->
        <section class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="pull-left margin-top-30 margin-bottom-20">&copy; <a href="http://www.facebook.com/" target="_blank">Copyright:Teamcolun</a>. All Rights Reserved.</p>
                    </div>    
                        <div class="col-md-6 col-sm-6 col-xs-6 payment">
                            <ul class="list-inline link">
                                <li class="link-social"><img src="assets/images/payment1.png" alt=""></li>
                                <li class="link-social"><img src="assets/images/payment2.png" alt=""></li>
                                <li class="link-social"><img src="assets/images/payment3.png" alt=""></li>
                                <li class="link-social"><img src="assets/images/payment4.png" alt=""></li>
                                <li class="link-social"><img src="assets/images/payment5.png" alt=""></li>
                            </ul>
                        </div>
                    
                </div>
            </div>
        </section>
        </footer>
    </div>
   </div> 

 <script type="text/javascript" src="assets/js/calkshoestore.js"></script>




</body>
</html>