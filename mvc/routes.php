<?php
define("PATH", "http://127.0.0.1/mvc");



$controllers = array(
	'pages'		=> ['home', 'error'],
	'product'	=> ['index','detail','search','categories','gender','color','price','genderandprice','genderandbrand','role','categoriesbrand'],
	'cart'		=> ['add','cart','clear','removeone','payment','addorder'],
	'customer'	=> ['login','proccess','register','logout','proccessregister']
);

if(!array_key_exists($controller, $controllers) || !in_array($action, $controllers[$controller])){
	$controller = "pages";
	$action = "error";
}



include_once "controllers/" . $controller . "_" . "controller.php";
$klass = str_replace("_", "", ucwords($controller,'_')). "Controller";

$controller = new $klass;
$controller->$action();

?>







