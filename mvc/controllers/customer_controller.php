<?php
/**
 * 
 */
require_once "models/customer.php";
require_once "controllers/base_controller.php";

class CustomerController extends BaseController
{
	public function __construct(){
		$this->folder = "customer";
	}

	public function login(){
		$this->render("login");
	}

	public function proccess(){
		if (isset($_POST['btnLogin'])) {

			$email = $_POST['txtMail'];
			$pass = $_POST['txtPass'];
				
			$data = CustomerModel::checkLogin($email,$pass);
			$_SESSION['logged_in'] = false;
			$_SESSION['confirm'] = false;
			if ($data==true) {
				$_SESSION['logged_in'] = true;
				$_SESSION['customer'] = $email;
				 
				header("location:".PATH);
			}else{

				$_SESSION['confirm'] = true;
				header("location:".PATH.'/?controller=customer&action=login');
			}
		}
	}

	public function logout(){
		session_unset($_SESSION['customer']);

		$_SESSION['logged_in'] = false;
		header("location:".PATH);
		
	}
	public function register(){
		$this->render("register");
		
	}

	public function proccessregister(){
		if (isset($_POST['btnRegister'])){
			$db_username = 'root';
			$db_password = '';
			$db_name = 'calkshoes';
			$db_host = 'localhost';
			$connecDB = mysqli_connect($db_host, $db_username, $db_password,$db_name);
			$username=$_POST['txtMail'];
			$results = mysqli_query($connecDB,"SELECT email FROM customer WHERE email='$username'");
 
    		//return total count
    		$username_exist = mysqli_num_rows($results); //total records
    		if($username_exist) {
        		$_SESSION['emailwrong']=true;
        		$this->render("register");

    		}else{
        		$data=CustomerModel::addcustomer();
					if($data=true){
						$_SESSION['success']=true;
						$this->render("login");
					}else{
						$_SESSION['success']=false;
					}
    		}


			

			
		}else{
			$_SESSION['success']=false;
			header("location:".PATH.'/?controller=customer&action=register');
		}	
		
	}
}
?>