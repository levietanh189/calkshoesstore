<?php
/**
 * 
 */
require_once "models/product.php";
require_once "models/cart.php";
require_once "models/customer.php";
require_once "controllers/base_controller.php";
class CartController extends BaseController
{
	public function __construct()
	{
		$this->folder = "cart";
	}

	public function add(){
		if(isset($_GET['modem'])){
			$modem = $_GET['modem'];
			$quantity = $_POST['slQuantity'];

			$row[] = ProductModel::getProductByModem1($modem);

			//Tạo cấu trúc mảng của mỗi phần tử trong Session Cart
			$item = array(
				//key            = value
				$row[0]['modem'] => array(
					'modem'		=> $row[0]["modem"],
					'image'		=> $row[0]["image"],
					'id'		=> $row[0]["id_product"],
					'name' 		=> $row[0]['name'],
					'price'		=> $row[0]['price'],
					'sale'		=> $row[0]['sale'],
					'quantity'	=> $quantity
				)
			);

			//Kiem tra SESSION cua gio hang xem da ton tai san pham muon mua hay chua
			if(!empty($_SESSION['cart'])){
				//Neu da co trong SESSION -> Chi viec tang so luong cua san pham do
				if(in_array($row[0]['modem'], array_keys($_SESSION['cart']))){
					//Tien hang duyet tung phan tu trong SESSION de kiem tra theo 'key'
					foreach ($_SESSION['cart'] as $key => $value) {
						if($row[0]['modem'] == $key){
							$_SESSION['cart'][$key]['quantity'] += $_POST['slQuantity'] ;
						}
					}
				}else{
					//Neu san pham can mua chua co trong gio hang -> Gop san pham moi vao SESSION
					$_SESSION['cart'] = array_merge($_SESSION['cart'],$item);
				}
			}else{
				//Neu SP muon mua chua co trong SESSION -> Them moi phan tu vao SESSION
				$_SESSION['cart'] = $item;
			}


			$this->render("cart");
		}
	}

	public function cart(){

		$this->render("cart");

	}

	public function clear(){
		unset($_SESSION['cart']);
		$_SESSION['count']=0; 

		
		$this->render("cart");
		
	}
	public function removeone(){
		if(isset($_GET['modem'])){
			$modem = $_GET['modem'];
			
			$_SESSION['count']-=1;
			$row[] = ProductModel::getProductByModem1($modem);

			//Tạo cấu trúc mảng của mỗi phần tử trong Session Cart
			$item = array(
				//key            = value
				$row[0]['modem'] => array(
					'modem'		=> $row[0]["modem"],
					'image'		=> $row[0]["image"],
					'id'		=> $row[0]["id_product"],
					'name' 		=> $row[0]['name'],
					'price'		=> $row[0]['price'],
					'sale'		=> $row[0]['sale'],
					
				)
			);

			//Kiem tra SESSION cua gio hang xem da ton tai san pham muon mua hay chua
			foreach ($_SESSION['cart'] as $cart_itm) //Vòng lặp biến SESSION
			   	 {
			        if($cart_itm['modem'] != $modem ){ //Lọc lại giỏ hàng, sản phẩm nào trùng product_id muốn xóa sẽ bị loại bỏ
			           $product[] = array('name'=>$cart_itm["name"], 'modem'=>$cart_itm["modem"], 'quantity'=>$cart_itm["quantity"], 'price'=>$cart_itm["price"], 'image'=>$cart_itm["image"], 'id'=>$cart_itm["id"], 'sale'=>$cart_itm["sale"]);
			        }
			        
			        //Tạo mới biến SESSION lưu giỏ hàng
			        if (isset($product)) {
			        	$_SESSION["cart"] = $product;

			        }else{
			        	unset($_SESSION['cart']);
			        }
			        
			    }

			$this->render("cart");
		}
	}

	public function payment(){
		

         if (isset($_SESSION['logged_in'])&& $_SESSION['logged_in']==true) {
         	$searchVal = $_SESSION['customer'];
			$data = CustomerModel::getCustomerByEmail($searchVal);
			
         	$this->render("payment",$data);
         }else{

		
		 	header("location:".PATH.'/?controller=customer&action=login');
		
		}
	}

	public function addorder(){
		

        			$data=CartModel::addorder();
					if($data=true){
						$_SESSION['successorder']=true;
						unset($_SESSION['cart']);	
						$_SESSION['count']=0;
						$this->render("cart");
						
					}else{
						$_SESSION['successorder']=false;
						$this->render("cart");
					}
	}



	
}
?>