<?php
/**
 * 
 */
require_once "models/product.php";
require_once "controllers/base_controller.php";

class ProductController extends BaseController
{
	public function __construct(){
		$this->folder = "product";
	}

	public function index(){
		$data = ProductModel::getAllProduct();
		$data1 = ProductModel::getAllSales();
		$data2 = ProductModel::getAllDate();
		$this->render("index",$data,$data1,$data2);
	}
	

	public function detail(){
		$id= isset($_GET['id']) ? $_GET['id'] : "";

		$data = ProductModel::getProductByModem($id);
		$data1 = ProductModel::getAllDate();
		
		
		$this->render("detail",$data,$data1);
	}

	public function search(){
		$searchVal = $_POST['txtSearch'];
		$data = ProductModel::search($searchVal);
		$this->render("result",$data);
	}

	public function categories(){
		
		
		$this->render("categories");
	}

	public function color(){
		$color = isset($_GET['color']) ? $_GET['color'] : "";
		$data = ProductModel::getProductByColor($color);
		$this->render("categories",$data);

	}

	public function gender(){
		$gender = isset($_GET['gender']) ? $_GET['gender'] : "";
		$data = ProductModel::getProductByGender($gender);
		$this->render("categories",$data);

	}

	public function price(){
		$min = isset($_GET['min']) ? $_GET['min'] : "";
		$max = isset($_GET['max']) ? $_GET['max'] : "";
		$data = ProductModel::getProductByPrice($min,$max);
		$this->render("categories",$data);
	}

	public function genderandprice(){
		$gender= isset($_GET['gender']) ? $_GET['gender'] : "";
		$min = isset($_GET['min']) ? $_GET['min'] : "";
		$max = isset($_GET['max']) ? $_GET['max'] : "";
		$data = ProductModel::getProductByGenderAndPrice($gender,$min,$max);
		$this->render("categories",$data);
	}
	public function genderandbrand(){
		$gender= isset($_GET['gender']) ? $_GET['gender'] : "";
		$brand=isset($_GET['brand']) ? $_GET['brand'] : "";
		$data = ProductModel::getProductByGenderAndBrand($gender,$brand);
		$this->render("categories",$data);
	}
	public function roleandprice(){
		$role= isset($_GET['role']) ? $_GET['role'] : "";
		$min = isset($_GET['min']) ? $_GET['min'] : "";
		$max = isset($_GET['max']) ? $_GET['max'] : "";
		$data = ProductModel::getProductByRolerAndPrice($role,$min,$max);
		$this->render("categories",$data);
	}
	public function role(){
		$role= isset($_GET['role']) ? $_GET['role'] : "";
		$data = ProductModel::getProductByRole($role);
		$this->render("categories",$data);
	}

	public function categoriesbrand(){
		
		$this->render("categories1");
	}
	
	



}
?>











