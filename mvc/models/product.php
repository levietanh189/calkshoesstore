<?php
/**
 * 
 */
class ProductModel
{
	private $productId;
	private $productName;
	//......


	public static function getAllProduct(){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product ORDER BY sold LIMIT 0,4";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}
	public static function getAllSales(){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product ORDER BY sale DESC LIMIT 0,2";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}
	public static function getAllDate(){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product ORDER BY time_in DESC LIMIT 0,4";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getProductByModem($id){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM product JOIN supplier ON product.id_supplier=supplier.id_supplier WHERE modem=:m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$id);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}



	public static function search($searchVal){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE name LIKE '%".$searchVal."%'";
		$stmt = $db->prepare($sql);
		//$stmt->bindParam(":n", $searchVal);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}


	public static function getProductByColor($color){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM product WHERE color like '%".$color."%'";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getProductByGender($gender){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE id_gender=:m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$gender);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getProductByPrice($min,$max){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE price BETWEEN :m and :n";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$min);
		$stmt->bindParam(":n",$max);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

		public static function getProductByGenderAndPrice($gender,$min,$max){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE id_gender = :l AND price BETWEEN :m and :n";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":l",$gender);
		$stmt->bindParam(":m",$min);
		$stmt->bindParam(":n",$max);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

		public static function getProductByGenderAndBrand($gender,$brand){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE id_gender = :l AND id_supplier = :m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":l",$gender);
		$stmt->bindParam(":m",$brand);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getProductByRoleAndBrand($role,$brand){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE id_role = :l AND id_supplier = :m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":l",$role);
		$stmt->bindParam(":m",$brand);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getProductByRoleAndPrice($gender,$min,$max){
		$items = [];
		$db = DB::getConnection();

		$sql = "SELECT * FROM product WHERE id_gender = :l AND price BETWEEN :m and :n";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":l",$role);
		$stmt->bindParam(":m",$min);
		$stmt->bindParam(":n",$max);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

	public static function getProductByRole($role){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM product WHERE id_role = :m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$role);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}
	public static function getProductByModem1($id){
		$db = DB::getConnection();
		$sql = "SELECT * FROM product WHERE modem=:m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$id);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			return $stmt->fetch(PDO::FETCH_ASSOC);
		}
	}



}
?>











