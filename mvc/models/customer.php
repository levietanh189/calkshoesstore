<?php
class CustomerModel
{
	public static function checkLogin($email, $pass){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM customer WHERE email=:e AND password=:p";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":e",$email);
		$stmt->bindParam(":p",$pass);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$_SESSION['customerid']=$row['id_customer'];
			}
			return true;
		}else{
			return false;
		}
	}

	public static function addcustomer(){
		$db = DB::getConnection();
			
		
			$sql = "INSERT INTO customer SET name=? ,email=? ,password=?,phone=?";
			$stmt = $db->prepare($sql);
			$stmt->bindParam("1",$_POST['txtUser']);
			$stmt->bindParam("2",$_POST['txtMail']);
			$stmt->bindParam("3",$_POST['txtPass']);
			$stmt->bindParam("4",$_POST['txtMobile']);
			
			$stmt->execute();
			$count = $stmt->rowCount();
			if($count>0){
				
				return true;
			}else{
				return false;
			}
		


		
		
		
	}

	public static function getCustomerByEmail($email){
		$items = [];
		$db = DB::getConnection();
		$sql = "SELECT * FROM customer WHERE email=:m";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(":m",$email);
		$stmt->execute();
		$count = $stmt->rowCount();
		if($count>0){
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				$items[] = $row;
			}
		}
		return $items;
	}

}
?>